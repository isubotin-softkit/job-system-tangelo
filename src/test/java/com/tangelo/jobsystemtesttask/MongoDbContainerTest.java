package com.tangelo.jobsystemtesttask;

import com.tangelo.jobsystemtesttask.constants.SchedulerJob;
import com.tangelo.jobsystemtesttask.entity.JobInstance;
import com.tangelo.jobsystemtesttask.entity.Status;
import com.tangelo.jobsystemtesttask.repository.JobInstanceRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MongoDbContainerTest extends BaseSpringBootTest {

    @Autowired
    private JobInstanceRepository jobInstanceRepository;

    @AfterEach
    void cleanUp() {
        this.jobInstanceRepository.deleteAll();
    }

    @Test
    void baseJobPersistence() {
        JobInstance jobInstance = new JobInstance();
        jobInstance.setJobKey(SchedulerJob.DAILY_JOB.name());
        jobInstance.setStatus(Status.NEW);
        jobInstance.setStartTime(LocalDateTime.now());
        jobInstanceRepository.save(jobInstance);
        assertEquals(1, jobInstanceRepository.findAll().size());
    }
}
