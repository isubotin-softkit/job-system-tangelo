package com.tangelo.jobsystemtesttask.config;

import com.tangelo.jobsystemtesttask.constants.SchedulerJob;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.Map;

import static com.tangelo.jobsystemtesttask.constants.SchedulerJob.*;

@Configuration
public class ScheduledConfig {

    @Value("${quartz.daily.cronExpression}")
    private String dailyCronExpression;

    @Value("${quartz.everyMinute.cronExpression}")
    private String everyMinuteCronExpression;

    private static final Map<SchedulerJob, String> SCHEDULE_CRON_EXPRESSIONS = new EnumMap<>(SchedulerJob.class);

    @PostConstruct
    public void initMap() {
        SCHEDULE_CRON_EXPRESSIONS.put(DAILY_JOB, this.dailyCronExpression);
        SCHEDULE_CRON_EXPRESSIONS.put(FAILED_JOB, this.everyMinuteCronExpression);
        SCHEDULE_CRON_EXPRESSIONS.put(TEST_JOB, this.everyMinuteCronExpression);
    }

    @Bean
    public Map<SchedulerJob, String> scheduleCronExpressions() {
        return SCHEDULE_CRON_EXPRESSIONS;
    }
}
