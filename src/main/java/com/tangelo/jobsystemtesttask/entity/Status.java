package com.tangelo.jobsystemtesttask.entity;


public enum Status {
    NEW,
    IN_PROGRESS,
    DONE,
    FAILED;
}
