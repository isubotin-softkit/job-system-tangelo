package com.tangelo.jobsystemtesttask.utils;

import org.quartz.CronExpression;

public class CronExpressionUtils {
    public static void validateExpression(String cronExpression) {
        try {
            CronExpression.validateExpression(cronExpression);
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Cron expression %s is not valid, because %s", cronExpression, e));
        }
    }
}
