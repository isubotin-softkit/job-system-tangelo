package com.tangelo.jobsystemtesttask.rest.dto;

import com.tangelo.jobsystemtesttask.constants.SchedulerJob;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuartzRequest {

    @ApiModelProperty(value = "Job", required = true)
    @NotNull(message = "Job can not be null.")
    private SchedulerJob job;
}
