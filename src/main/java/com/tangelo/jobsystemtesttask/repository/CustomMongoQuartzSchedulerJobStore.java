package com.tangelo.jobsystemtesttask.repository;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.novemberain.quartz.mongodb.MongoDBJobStore;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;

import javax.el.PropertyNotFoundException;
import java.util.Properties;

public class CustomMongoQuartzSchedulerJobStore extends MongoDBJobStore {

    private static final String DEFAULT_APP_CONFIG_FILE_PATH = "config/application.yml";
    private static String mongoAddresses;
    private static String dbName;

    public CustomMongoQuartzSchedulerJobStore() {
        super();
        initializeMongo();
        setMongoUri(mongoAddresses);
        setDbName(dbName);
    }

    /**
     * <p>
     * This method will initialize the mongo instance required by the Quartz scheduler.
     * </p>
     */
    private void initializeMongo() {
        YamlPropertiesFactoryBean properties = getPropertiesFactoryClassPath(DEFAULT_APP_CONFIG_FILE_PATH);

        Properties propertiesObject = properties.getObject();
        if (propertiesObject != null) {
            mongoAddresses = propertiesObject.getProperty("spring.data.mongodb.uri");
            try (MongoClient mongoClient = MongoClients.create(mongoAddresses)) {
                dbName = mongoClient.getDatabase("jobs").getName();
            }
        } else {
            throw new PropertyNotFoundException("Properties not found");
        }

    }

    private YamlPropertiesFactoryBean getPropertiesFactoryClassPath(String file) {
        YamlPropertiesFactoryBean bootstrapProperties = new YamlPropertiesFactoryBean();
        ClassPathResource bootstrapResource = new ClassPathResource(file);
        bootstrapProperties.setResources(bootstrapResource);
        return bootstrapProperties;
    }

}
