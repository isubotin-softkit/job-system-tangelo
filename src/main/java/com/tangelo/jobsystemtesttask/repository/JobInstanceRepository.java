package com.tangelo.jobsystemtesttask.repository;

import com.tangelo.jobsystemtesttask.entity.JobInstance;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobInstanceRepository extends MongoRepository<JobInstance, ObjectId> {

}
