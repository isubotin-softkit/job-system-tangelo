package com.tangelo.jobsystemtesttask.service.job;

import com.tangelo.jobsystemtesttask.constants.SchedulerJob;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.PersistJobDataAfterExecution;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Slf4j
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
@Component
public class TestJob extends QuartzJob {

    @Override
    public void accept(JobExecutionContext basicJobContext) {

        log.info("Starting test job at {}", Instant.now());
    }

    @Override
    protected SchedulerJob getJobType() {
        return SchedulerJob.TEST_JOB;
    }

}
